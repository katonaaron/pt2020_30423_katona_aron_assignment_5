#!/bin/bash

jar="PT2020_30423_Katona_Aron_Assignment_5.jar"
nr_tasks=6

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

echo -ne "${BLUE}Running \"$jar\" ... "

# Running the jar and checking the return value
if java -jar "$jar"
then
  echo -e "${GREEN}"Program terminated successfully.
else
  >&2 echo -e "${RED}"Program terminated with error.
  exit 1
fi

# Checking if the output files exist
for i in $(seq 1 $nr_tasks)
do
  file=Task_${i}.txt
  if [ ! -f "$file" ]
  then
    >&2 echo -e The output file \""$file"\" does not exists.
    exit 1
  fi
done

# Testing task 1
echo -ne "${BLUE}Testing task 1 ... "

if o=$(diff -Z Activities.txt Task_1.txt)
then
  echo -e "${GREEN}"PASSED
else
  echo -e "${RED}"FAILED
  >&2 echo Task_1.txt differs from Activities.txt:
  >&2 echo "$o"
  exit 1
fi

# Testing task 2
echo -e "${BLUE}Testing task 2 ... ${NC}"MANUAL
# TODO
echo Actual: "$(cat Task_2.txt)"

# Testing task 3
echo -ne "${BLUE}Testing task 3 ... "

expected=$(cut -d$'\t' -f5 Activities.txt | sort | uniq -c)
actual=$(sort Task_3.txt | uniq | awk ' { t = $1; $1 = $2; $2 = t; print; } ')

if o=$(diff -w <(echo "$expected") <(echo "$actual"))
then
  echo -e "${GREEN}"PASSED
else
  echo -e "${RED}"FAILED
  >&2 echo Task_1.txt differs from Activities.txt:
  >&2 echo "$o"
  exit 1
fi

# Testing task 4
echo -e "${BLUE}Testing task 4... ${NC}"MANUAL
# TODO

# Testing task 5
echo -e "${BLUE}Testing task 5... ${NC}"MANUAL
# TODO
echo -e Actual: "\n$(cat Task_5.txt)"

# Testing task 6
echo -e "${BLUE}Testing task 6... ${NC}"MANUAL
# TODO
echo -e Actual: "\n$(cat Task_6.txt)"

# All tests passed
echo -e "${GREEN}"All tests passed


 \documentclass[a4paper,10pt]{article}
\usepackage{fontspec}
\usepackage[singlespacing]{setspace}
\usepackage{listings}
\usepackage{color}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{enumitem}
\usepackage{listings}
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}
\setmainfont{Times New Roman}

\usepackage{biblatex}
\usepackage{url}
\setcounter{biburllcpenalty}{7000}
\setcounter{biburlucpenalty}{8000}
\addbibresource{bibliography.bib}

\newcommand{\subsubsubsection}[1]{\paragraph{#1}\mbox{}\\}
\setcounter{secnumdepth}{4}
\setcounter{tocdepth}{3}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=left,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3,
  escapechar=|
}

%opening
\title{Processing Sensor Data Of Daily Living Activities}
\author{Katona Áron}

\begin{document}

\pagenumbering{gobble}
\maketitle

\newpage
\pagenumbering{roman}
\tableofcontents

\newpage
\pagenumbering{arabic}

\section{Objective}
The main objective is to design, implement and test an application for analyzing the behavior of a person, recorded by a set of sensors installed in its house. 

Functional programming and stream processing are used in order to produce the requested results.

\subsection{Secondary objectives}

The main objective can be divided into multiple independent steps:

\begin{itemize}
    
\item \textbf{Creating an appropriate model for storing the data}
\hyperref[sec:model]{(\ref{sec:model})}
    
\item \textbf{Reading data from an input file} 
\hyperref[sec:app]{(\ref{sec:app})}

\item \textbf{Transforming the read data into a processable format}
\hyperref[sec:task1]{(\ref{sec:task1})}

\item \textbf{Executing the required tasks by using functional programming and stream processing} 
\hyperref[sec:tasks]{(\ref{sec:tasks})}

\item \textbf{Formatting the results}
\hyperref[sec:formatter]{(\ref{sec:formatter})}

\item \textbf{Writing results into separate text files}
\hyperref[sec:writer]{(\ref{sec:writer})}

\end{itemize}











\section{Problem analysis and use cases}


\subsection{Problem analysis}
\label{sec:analysis}

The objective is to process a large number of sensor data. The data contains the name and the time interval of each activity. On this data set the following tasks are executed:

\begin{enumerate}[label=\arabic*.]

\item Create a model (\verb|MonitoredData|) which can hold the attributes of an activity, and transform the lines of the input file into a list of objects of this type.

\item Count the distinct days that appear in the monitoring data.

\item Count how many times each activity has appeared over the entire monitoring period.

\item Count for how many times each activity has appeared for each day over the
monitoring period.

\item For each activity compute the entire duration over the monitoring period.

\item \label{item:last_task} Get the name of the activities whose duration is less than 5 minutes in more than 90\% of their occurrence.
 
\end{enumerate}





\subsection{Input}
\label{sec:input_format}

The input file has the name \verb|Activities.txt|. Each of its lines corresponds to an activity, having the following format:
\begin{center}
 \verb|<start_time>\t\t<end_time>\t\t<activity_label>|
\end{center}

The \verb|start_time| and \verb|end_time| fields represent the period on which the given activity was performed. These values have the following format: 
\begin{center}
\verb*|yyyy-MM-dd HH:mm:ss|
\end{center}

The \verb|activity_label| field is the name of the activity, and \verb|\t| is the tab character.





\subsection{Output}
\label{sec:output_format}

After the execution of each task, the result is written into the corresponding text file. The file has the name:
\begin{center}
\verb|Task_<task_number>.txt|
\end{center}
Where \verb|task_number| is the identifier of the task, in the range between 1 and \ref{item:last_task}






\subsection{Use cases}

\textbf{Use Case:} Execute the tasks on a data set\\
\textbf{Primary Actor:} User\\
\textbf{Main Success Scenario:}

\begin{enumerate}

\item \label{step:1} The user creates an input file containing the sensor data and places it next to the jar. The file should correspond to the format described in (\ref{sec:input_format}).


\item The user calls the program in the terminal using the following command:
\begin{center}
\verb|java -jar PT2020_30423_Aron_Katona_Assignment_5.jar|
\end{center}
Where the \verb|<input_file>| is the name of the file created in step \ref{step:1}.


\item The program generates a text file for each executed task, the output having the format described in (\ref{sec:output_format}).

\item The program exits without any error message.
\end{enumerate}

\textbf{Alternative Sequences:}
\begin{enumerate}
\item File IO errors
\begin{itemize}
    \item Occurs when a file could not be opened, written into or read from.
    \item The program exits with an error message printed to the terminal (STDERR).
\end{itemize}
\item The input file has an invalid format
\begin{itemize}
    \item The program exits with an error message printed to the terminal (STDERR).
\end{itemize}
\end{enumerate}
    
    
    
    
    



\section{Design}

\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=\linewidth]{figures/class_diagram.pdf}
    \caption{Class diagram} 
    \label{fig:class_diagram}
\end{figure*}

On figure \ref{fig:class_diagram} on can see the class diagram of the application. 

The \verb|MonitoredData| model is created by the \verb|Tasks| class, which also processes it. Another two utility classes were created for formatting the result and writing it into text files. The main \verb|App| calls their methods and handles the exceptions.








\section{Implementation}



\subsection{MonitoredData}
\label{sec:model}

\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=.6\linewidth]{figures/model.pdf}
    \caption{The MonitoredData class} 
    \label{fig:model}
\end{figure*}

This model class holds the three attributes of an activity: the start time, end time and the name of the activity. Besides the regular getter methods A few date operations were also added:

\begin{itemize}

\item The \verb|getStartTime()| and \verb|getEndTime()| methods return directly immutable objects of type \verb|LocalDateTime|, representing the endpoints of the interval when the activity occurred. These objects contain both the day and the time.

\item The \verb|getStartDate()| and \verb|getEndDate()| methods are similar, with the exception that they return a \verb|LocalDate| object, containing only the day \cite{howtodoinjava:1}.

\item The \verb|getDuration()| method returns a \verb|Duration| object which contains the difference in between the end time and the start time.
 
\end{itemize}

The class is immutable.






\subsection{Tasks}
\label{sec:tasks}

\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=\linewidth]{figures/tasks.pdf}
    \caption{The Tasks class} 
    \label{fig:tasks}
\end{figure*}

This utility class contains the static methods for processing the input data. Stream processing and functional programming was used to obtain the results.






\subsubsection{Task 1 - Convert data}
\label{sec:task1}

Tasks one consists of transforming the lines of the input files into \verb|MonitoredData| objects. In order to do this the following was done:

\begin{enumerate}

\item Split the line into the three main parts (line \ref{line:split}).

\item Parse the dates into a \verb|LocalDateTime| object (line \ref{line:parse1}-\ref{line:parse2}).

\item Instantiate the \verb|MonitoredData| object (line \ref{line:new}).

\item Collect the results into a list (line \ref{line:toList}).
 
\end{enumerate}

\begin{lstlisting}[title={convertData method - Task 1}]
public static List<MonitoredData> convertData( Stream<String> lineStream, DateTimeFormatter dateTimeFormatter) {
    return lineStream
        .map(line -> line.split("\t\t"))|\label{line:split}|
        .map(tokens -> new MonitoredData(|\label{line:new}|
                LocalDateTime.parse(tokens[0], dateTimeFormatter),|\label{line:parse1}|
                LocalDateTime.parse(tokens[1], dateTimeFormatter),|\label{line:parse2}|
                tokens[2].trim()
            )
        )
        .collect(Collectors.toList());|\label{line:toList}|
} 
\end{lstlisting}







\subsubsection{Task 2 - Count the distinct days}
\label{sec:task2}

Tasks two requires to return the number of distinct days that appear in the sensor data. It was assumed, that if an activity overlaps on multiple days, then all the days between the start and end time (inclusive) needs to be counted. Thus I used the \verb|getDistinctDays| private method (\ref{sec:getDistinctDays}) two obtain a stream of these days. 

\begin{lstlisting}[title={countDistinctDays method - Task 2}]
public static long countDistinctDays(List<MonitoredData> dataList) {
    return dataList.stream()
        .flatMap(data -> |\label{line:flatmap}|
            getDistinctDays(data.getStartDate(), data.getEndDate())|\label{line:getDD}|
        )
        .distinct()|\label{line:distinct}|
        .count();|\label{line:count}|
}
\end{lstlisting}

The following steps were taken:

\begin{enumerate}

\item For each activity obtain the days during which it was recorded (line \ref{line:getDD}).

\item Flatten the streams (line \ref{line:flatmap}).

\item Obtain the distinct days (line \ref{line:distinct}).

\item Count the number of resulted days  (line \ref{line:count}).
 
\end{enumerate}







\subsubsection{Task 3 - Get the frequency of each activity}
\label{sec:task3}

Tasks three requires to return the number of apparition of each activity over the entire monitoring period.

To compute the result, the following steps were taken:

\begin{enumerate}

\item Group the monitored data by the activity name (line \ref{line:groupBy}-\ref{line:getActivity}).

\item Count the number of records in the group (line \ref{line:summingInt}).
 
\end{enumerate}


\begin{lstlisting}[title={getActivityFrequency method - Task 3}]
public static Map<String, Integer> getActivityFrequency(List<MonitoredData> dataList) {
    return dataList.stream()
        .collect(
            Collectors.groupingBy(|\label{line:groupBy}|
                MonitoredData::getActivity,|\label{line:getActivity}|
                Collectors.summingInt(e -> 1)|\label{line:summingInt}|
            )
        );
}
\end{lstlisting}













\subsubsection{Task 4 - For each day get the frequency of the activities}
\label{sec:task4}

Task four is similar to Task 3 (\ref{sec:task3}), again the number of apparition needs to be calculated for each activity. The difference is that now we calculate this number separately for each day, and return a Map which associates the days to these results.

It is specified that a \verb|Map<Integer, Map<String, Integer>>| needs to be returned. Thus it is assumed that each day is identified by a unique number, starting from 1 until the total number of distinct days in the data set, i.e. the value returned by Task 2 (\ref{sec:task2}).

This task was split into two methods in order to increase readability and reusability.





\subsubsubsection{getDailyActivityFrequency}
\label{sec:daily}

The method obtains the daily frequency of each activity, but the days in the result are specified by a \verb|LocalDate| object and instead of a number.

To compute the result, the following steps were taken:

\begin{enumerate}

\item For each activity obtain the distinct days in its time interval (line \ref{line:getDD2}).

\item Associate to each day the name of the activity (line \ref{line:mapActivity}).

\item Flatten the stream, thus treating each key-value pair equally (line \ref{line:flatMap2}).

\item Group the results by the day (line \ref{line:groupByDate}).

\item Group the results by the name of the activity (line \ref{line:groupByName}).

\item Count the number of records, i.e. the number of times this activity is present for the given day (line \ref{line:count2}).
 
\end{enumerate}


\begin{lstlisting}[title={getDailyActivityFrequency method - Task 4}]
public static Map<LocalDate, Map<String, Integer>> getDailyActivityFrequency(List<MonitoredData> dataList) {
    return dataList.stream()
        .flatMap(|\label{line:flatMap2}|
            data -> getDistinctDays(data.getStartDate(), data.getEndDate())|\label{line:getDD2}|
                    .map(day -> new AbstractMap.SimpleEntry<>(day, data.getActivity()))|\label{line:mapActivity}|
        )
        .collect(
            Collectors.groupingBy(
                Map.Entry::getKey,|\label{line:groupByDate}|
                Collectors.groupingBy(
                        Map.Entry::getValue,|\label{line:groupByName}|
                        Collectors.summingInt(e -> 1)|\label{line:count2}|
                )
            )
        );
}
\end{lstlisting}





\subsubsubsection{getNumberedDailyActivityFrequency}
The method transforms the result obtained in (\ref{sec:daily}), such that the days are identified by their sequential number instead of a \verb|LocalDate| object \cite{so:2}.

To compute the result, the following steps were taken:

\begin{enumerate}

\item Obtain the result from (\ref{sec:daily}) (line \ref{line:obtainResult}).

\item Sort the result by its date (line \ref{line:sort}).

\item Collect the result associated to each day in a list (line \ref{line:map}-\ref{line:collect}).

\end{enumerate}

Now this list contains the frequency of activities for each day, and the days are identified by their index in the sorted list i.e. by their sequential number.

\begin{enumerate}

\setcounter{enumi}{3}

\item Create a stream of numbers corresponding to the indices of the list (line \ref{line:intStream}).

\item Associate the elements of the list to the indices (line \ref{line:collectIntStream}).
 
\end{enumerate}

\begin{lstlisting}[title={getNumberedDailyActivityFrequency method - Task 4}]
public static Map<Integer, Map<String, Integer>> getNumberedDailyActivityFrequency(List<MonitoredData> dataList) {
    final List<Map<String, Integer>> dailyActivities = getDailyActivityFrequency(dataList)|\label{line:obtainResult}|
        .entrySet().stream()
        .sorted(Map.Entry.comparingByKey())|\label{line:sort}|
        .map(Map.Entry::getValue)|\label{line:map}|
        .collect(Collectors.toList());|\label{line:collect}|

    return IntStream
        .range(0, dailyActivities.size())|\label{line:intStream}|
        .boxed()
        .collect(Collectors.toMap(i -> i + 1, dailyActivities::get));|\label{line:collectIntStream}|
}
\end{lstlisting}







\subsubsection{Task 5 - Get the total duration of each activity}

Tasks five requires to return the total duration of each activity over the entire monitoring period. This was achieved by summing the duration of each apparition of an activity.

The specification requested that a \verb|Map<String, LocalTime>| object should be returned. Because the maximum value a \verb|LocalTime| object can hold is \verb|23:59:59.99|\dots, I needed the choose another class to represent the duration. Thus I chose the \verb|Duration| class, which can store even larger time periods. This change in type was needed, also because the input data had three activities with larger total duration than 24 hours.

To compute the result, the following steps were taken:

\begin{enumerate}

\item Group the monitored data by the activity name (line \ref{line:groupByActivity2}).

\item Sum the duration for each apparition of the activity (line \ref{line:sumDurationStart}-\ref{line:sumDurationEnd}).
 
\end{enumerate}


\begin{lstlisting}[title={getTotalActivityDuration method - Task 5}]
 public static Map<String, Duration> getTotalActivityDuration(List<MonitoredData> dataList) {
    return dataList.stream()
        .collect(Collectors.groupingBy(
            MonitoredData::getActivity,|\label{line:groupByActivity2}|
            Collectors.mapping(|\label{line:sumDurationStart}|
                MonitoredData::getDuration,
                Collectors.collectingAndThen(
                    Collectors.reducing(Duration::plus),
                    Optional::orElseThrow
                )
            )|\label{line:sumDurationEnd}|
        ));
}
\end{lstlisting}








\subsubsection{Task 6 - Get the short activities}

Tasks 6 requires to return the name of the activities whose duration is less than 5 minutes in more than 90\% of their occurrence.

To compute the result, I extended the \verb|getTotalActivityDuration()| method of Task 3 (\ref{sec:task3}), by giving an additional \verb|Duration| parameter: the upper bound for duration. Thus all activities will be filtered out if their duration is $\ge$ given parameter (line \ref{line:filterDuration}). 

\begin{lstlisting}[title={Extended getActivityFrequency method - Task 6}]
public static Map<String, Integer> getActivityFrequency(List<MonitoredData> dataList, Duration durationBound) {
    return dataList.stream()
        .filter(data -> data.getDuration().compareTo(durationBound) < 0)|\label{line:filterDuration}|
        .collect(
            Collectors.groupingBy(
                MonitoredData::getActivity, 
                Collectors.summingInt(e -> 1)
            )
        );
}
\end{lstlisting}

By using the two versions of the previous method, I can compute the percent of short activities using the following formula:

\begin{equation}\label{eq:1}
  \frac{number\:of\:short\:activities}{number\:of\:total\:activities}*100\;[\%]
\end{equation}


Thus the responsibility of this task is to filter out the activities for which the previous formula (\ref{eq:1}) gives a percentage $\le$ 90\%, and return the names of the remaining ones.

To compute the result, the following steps were taken:

\begin{enumerate}

\item Obtain the total number of occurrence of each activity (line \ref{line:obtainTotal}).

\item Obtain the number of short occurrence of each activity (line \ref{line:obtainShort}).

\item Filter the activities using formula (\ref{eq:1}) (line \ref{line:filterPercentage}).

\item Obtain the names of the activities (line \ref{line:mapActivity3}).

\item Filter out the repetitions (line \ref{line:distinct2}).

\item Collect the distinct names into a list (line \ref{line:toList2}).

 
\end{enumerate}


\begin{lstlisting}[title={getShortActivities method - Task 6}]
public static List<String> getShortActivities(List<MonitoredData> dataList, Duration durationBound, double percentageBound) {
    final Map<String, Integer> totalActivityFrequency = getActivityFrequency(dataList);|\label{line:obtainTotal}|

    return getActivityFrequency(dataList, durationBound)|\label{line:obtainShort}|
        .entrySet().stream()
        .filter(entry -> 1.0 * entry.getValue() / totalActivityFrequency.get(entry.getKey()) * 100 > percentageBound)|\label{line:filterPercentage}|
        .map(Map.Entry::getKey)|\label{line:mapActivity3}|
        .distinct()|\label{line:distinct2}|
        .collect(Collectors.toList());|\label{line:toList2}|
}
\end{lstlisting}










\subsubsection{Distinct days of a time interval}
\label{sec:getDistinctDays}

This private method was created for generating a stream of days of type \verb|LocalDate| between the two intervals given as parameters \cite{so:1}. The method is used when the total number of days are are required. 

\begin{enumerate}

\item Starting from the first date, the days are incremented until the second date is reached

\item Each day is added to a newly created ArrayList. 

\item Finally a stream is created from the list.

\end{enumerate}

\begin{lstlisting}[title={getDistinctDays method}, escapechar=`]
private static Stream<LocalDate> getDistinctDays(LocalDate from, LocalDate to) {
    final List<LocalDate> result = new ArrayList<>();
    LocalDate day = from;

    while (day.isBefore(to) || day.isEqual(to)) {
        result.add(day);
        day = day.plusDays(1);
    }

    return result.stream();
}
\end{lstlisting}
















\subsection{Formatter}
\label{sec:formatter}

\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=\linewidth]{figures/formatter.pdf}
    \caption{The Formatter class} 
    \label{fig:formatter}
\end{figure*}

The formatter class transforms the results of the tasks into a string or into a list of strings.

\begin{itemize}

\item \verb|formatMonitoredData()| - formats the result of Task 1 (\ref{sec:task1}).

\item \verb|formatDailyActivities()| - formats the result of Task 4 (\ref{sec:task4}).

\item \verb|formatMap()| - formats a generic string. If no functions are given for converting the entries of the map to string, the \verb|Object.toString()| method will be used.

\item \verb|formatDuration()| - converts a \verb|Duration| object to string by using the time format: \verb|HH:mm:ss|.

\item \verb|mergeColumns()| - aligns two columns and merges them into a single list of strings.
 
\end{itemize}
















\subsection{StringFileWriter}
\label{sec:writer}

This class writes a string or a list of strings into the file whose path is given as the first argument.

\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=.5\linewidth]{figures/writer.pdf}
    \caption{The StringFileWriter class} 
    \label{fig:writer}
\end{figure*}










\subsection{The main class}
\label{sec:app}

\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=.5\linewidth]{figures/app.pdf}
    \caption{The main class} 
    \label{fig:app}
\end{figure*}

This class holds the input data after it was transformed into a list of objects. Also it contains constants for configuration.

In the \verb|main()| method the data is read from the files \cite{mkyong:1}, transformed and processed using the methods of the \verb|Tasks| class. Finally by using the \verb|StringFileWriter| and \verb|Formatter| classes, the resuls are written into text files.








\section{Result}

A shell script was written for testing the outputs of the program. The requested tasks were performed using Linux commands and the outputs were compared.

I also verified the correct behavior of the program for the cases when an activity overlaps for multiple days or if for some days no measurements were taken.








\section{Conclusion}

This assignment was a good opportunity to learn about functional programming and working with streams. One can observe that, by using this technique, with a few lines of code results can be achieved that would take a much longer time to be produced by using imperative programming. Also the this approach is thread safe, parallelizable, more compact, and the chance of error is lower.

The application can be further developed by:
\begin{itemize}
 \item creating new tasks for processing the input data.
 \item creating a mechanism for real time data processing, i.e. the results being updated at the same time as the data is received from the sensors.
\end{itemize}

\nocite{*}
\printbibliography

\end{document}

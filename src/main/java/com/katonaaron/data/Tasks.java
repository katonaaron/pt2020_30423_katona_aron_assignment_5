package com.katonaaron.data;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public final class Tasks {
    private Tasks() {
    }

    public static List<MonitoredData> convertData(Stream<String> lineStream, DateTimeFormatter dateTimeFormatter) {
        return lineStream
                .map(line -> line.split("\t\t"))
                .map(tokens -> new MonitoredData(
                                LocalDateTime.parse(tokens[0], dateTimeFormatter),
                                LocalDateTime.parse(tokens[1], dateTimeFormatter),
                                tokens[2].trim()
                        )
                )
                .collect(Collectors.toList());
    }

    public static long countDistinctDays(List<MonitoredData> dataList) {
        return dataList.stream()
                .flatMap(data -> getDistinctDays(data.getStartDate(), data.getEndDate()))
                .distinct()
                .count();
    }

    public static Map<String, Integer> getActivityFrequency(List<MonitoredData> dataList) {
        return dataList.stream()
                .collect(
                        Collectors.groupingBy(
                                MonitoredData::getActivity,
                                Collectors.summingInt(e -> 1)
                        )
                );
    }

    public static Map<String, Integer> getActivityFrequency(List<MonitoredData> dataList, Duration durationBound) {
        return dataList.stream()
                .filter(data -> data.getDuration().compareTo(durationBound) < 0)
                .collect(
                        Collectors.groupingBy(
                                MonitoredData::getActivity,
                                Collectors.summingInt(e -> 1)
                        )
                );
    }

    public static Map<LocalDate, Map<String, Integer>> getDailyActivityFrequency(List<MonitoredData> dataList) {
        return dataList.stream()
                .flatMap(
                        data -> getDistinctDays(data.getStartDate(), data.getEndDate())
                                .map(day -> new AbstractMap.SimpleEntry<>(day, data.getActivity()))
                )
                .collect(
                        Collectors.groupingBy(
                                Map.Entry::getKey,
                                Collectors.groupingBy(
                                        Map.Entry::getValue,
                                        Collectors.summingInt(e -> 1)
                                )
                        )
                );
    }

    public static Map<Integer, Map<String, Integer>> getNumberedDailyActivityFrequency(List<MonitoredData> dataList) {
        final List<Map<String, Integer>> dailyActivities = getDailyActivityFrequency(dataList)
                .entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());

        return IntStream
                .range(0, dailyActivities.size())
                .boxed()
                .collect(Collectors.toMap(i -> i + 1, dailyActivities::get));
    }

    public static Map<String, Duration> getTotalActivityDuration(List<MonitoredData> dataList) {
        return dataList.stream()
                .collect(Collectors.groupingBy(
                        MonitoredData::getActivity,
                        Collectors.mapping(
                                MonitoredData::getDuration,
                                Collectors.collectingAndThen(
                                        Collectors.reducing(Duration::plus),
                                        Optional::orElseThrow
                                )
                        )
                ));
    }

    public static List<String> getShortActivities(List<MonitoredData> dataList, Duration durationBound, double percentageBound) {
        final Map<String, Integer> totalActivityFrequency = getActivityFrequency(dataList);

        return getActivityFrequency(dataList, durationBound).entrySet().stream()
                .filter(entry -> 1.0 * entry.getValue() / totalActivityFrequency.get(entry.getKey()) * 100 > percentageBound)
                .map(Map.Entry::getKey)
                .distinct()
                .collect(Collectors.toList());
    }

    private static Stream<LocalDate> getDistinctDays(LocalDate from, LocalDate to) {
        final List<LocalDate> result = new ArrayList<>();
        LocalDate day = from;

        while (day.isBefore(to) || day.isEqual(to)) {
            result.add(day);
            day = day.plusDays(1);
        }

        return result.stream();
    }
}

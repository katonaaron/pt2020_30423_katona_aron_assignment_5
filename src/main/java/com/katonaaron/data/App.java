package com.katonaaron.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Stream;

public class App {
    private static final String INPUT_FILE_PATH = "Activities.txt";
    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
    private static final double PERCENTAGE_BOUND = 90.0;
    private static final Duration DURATION_BOUND = Duration.parse("PT5M");

    private static List<MonitoredData> data;

    public static void main(String[] args) {

        try (Stream<String> lines = Files.lines(Path.of(INPUT_FILE_PATH))) {
            data = Tasks.convertData(lines, DATE_TIME_FORMATTER);
        } catch (IOException e) {
            System.err.println("Error: File could not be read: " + e);
            System.exit(1);
        }

        try {
            StringFileWriter.write("Task_1.txt", Formatter.formatMonitoredData(data, DATE_TIME_FORMATTER));
            StringFileWriter.write("Task_2.txt", Long.toString(Tasks.countDistinctDays(data)));
            StringFileWriter.write("Task_3.txt", Formatter.formatMap(Tasks.getActivityFrequency(data)));
            StringFileWriter.write("Task_4.txt", Formatter.formatDailyActivities(Tasks.getNumberedDailyActivityFrequency(data)));
            StringFileWriter.write("Task_5.txt", Formatter.formatMap(Tasks.getTotalActivityDuration(data), String::toString, Formatter::formatDuration));
            StringFileWriter.write("Task_6.txt", Tasks.getShortActivities(data, DURATION_BOUND, PERCENTAGE_BOUND));
        } catch (IOException e) {
            System.err.println("Error: Task result could not be written: " + e);
            System.exit(1);
        }
    }
}

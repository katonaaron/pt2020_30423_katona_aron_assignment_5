package com.katonaaron.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public final class StringFileWriter {
    public static void write(String path, String string) throws IOException {
        Files.write(Path.of(path), string.getBytes());
    }

    public static void write(String path, Iterable<String> string) throws IOException {
        Files.write(Path.of(path), string);
    }
}

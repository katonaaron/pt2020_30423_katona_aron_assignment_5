package com.katonaaron.data;

import java.time.Duration;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class Formatter {

    private Formatter() {
    }

    public static List<String> formatMonitoredData(List<MonitoredData> data, DateTimeFormatter dateTimeFormatter) {
        return data.stream()
                .map(d -> d.getStartTime().format(dateTimeFormatter) + "\t\t" + d.getEndTime().format(dateTimeFormatter) + "\t\t" + d.getActivity())
                .collect(Collectors.toList());
    }

    public static List<String> formatDailyActivities(Map<Integer, Map<String, Integer>> dailyActivities) {
        return dailyActivities.entrySet().stream()
                .map(entry -> "Day " + entry.getKey() + "\n"
                        + String.join("\n", formatMap(entry.getValue()))
                        + "\n"
                )
                .collect(Collectors.toList());
    }

    public static <K, V> List<String> formatMap(Map<K, V> map, Function<K, String> keyFormatter, Function<V, String> valueFormatter) {
        return mergeColumns(
                map.entrySet().stream()
                        .map(entry -> new AbstractMap.SimpleEntry<>(
                                        keyFormatter.apply(entry.getKey()),
                                        valueFormatter.apply(entry.getValue())
                                )
                        )
                        .collect(Collectors.toList())
        );
    }

    public static <K, V> List<String> formatMap(Map<K, V> map) {
        return formatMap(map, Objects::toString, Objects::toString);
    }

    public static String formatDuration(Duration duration) {
        return String.format("%d:%02d:%02d", duration.toHours(), duration.toMinutesPart(), duration.toSecondsPart());
    }

    private static List<String> mergeColumns(List<Map.Entry<String, String>> columns) {
        final int maxStringLength = columns.stream()
                .map(Map.Entry::getKey)
                .mapToInt(String::length)
                .max()
                .orElse(0);
        final String format = "%-" + maxStringLength + "s\t%s";

        return columns.stream()
                .map(entry -> String.format(format, entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }
}

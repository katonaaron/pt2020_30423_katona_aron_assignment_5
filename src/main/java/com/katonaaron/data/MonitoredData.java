package com.katonaaron.data;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

public class MonitoredData {
    private final LocalDateTime startTime;
    private final LocalDateTime endTime;
    private final String activity;

    public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDate getStartDate() {
        return startTime.toLocalDate();
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public LocalDate getEndDate() {
        return endTime.toLocalDate();
    }

    public Duration getDuration() {
        return Duration.between(getStartTime(), getEndTime());
    }

    public String getActivity() {
        return activity;
    }

    @Override
    public String toString() {
        return "MonitoredData{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                ", activity='" + activity + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MonitoredData)) return false;
        MonitoredData that = (MonitoredData) o;
        return Objects.equals(getStartTime(), that.getStartTime()) &&
                Objects.equals(getEndTime(), that.getEndTime()) &&
                Objects.equals(getActivity(), that.getActivity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStartTime(), getEndTime(), getActivity());
    }
}
